using System;

namespace MyMusicBagConsole
{
    public class MusicCard
    {
        public int id { get; set; }
        public string Artist { get; set; }
        public string Album { get; set; }
        public string Comment { get; set; }
        public string Link { get; set; }
        public override string ToString()
        {
            return String.Format("ID: {0}\nArtist: {1}; Album: {2}; Link: {4}\nComment: {3}",
            id, Artist, Album, Comment, Link);
        }
    }
}
