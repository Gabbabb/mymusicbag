﻿namespace MyMusicBagConsole
{
    class Program
    { 
        public static void Main(string[] args)
        {
            Provider provider = new Provider();
            ConsoleInput input = new ConsoleInput();
            App app = new App(provider, input);
            app.DoSmthWithConsoleInput(args);
        }
    }
}
