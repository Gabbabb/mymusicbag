using Microsoft.EntityFrameworkCore;

namespace MyMusicBagConsole
{
    public class MusicCardContext : DbContext
    {
        public MusicCardContext()
        {
            Database.EnsureCreated();
        }
        public DbSet<MusicCard> MusicCards { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=MyMusic.db");
        }
    }
}
