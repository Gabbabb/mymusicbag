using System;

namespace MyMusicBagConsole
{
    public class ConsoleInput
    {
        public MusicCard CreateMusicCardFromConsole()
        {
            Console.Write("Artist: ");
            string artist = Console.ReadLine();
            Console.Write("Album: ");
            string album = Console.ReadLine();
            Console.Write("Comment: ");
            string comment = Console.ReadLine();
            Console.Write("Yandex.Music Link: ");
            string link = Console.ReadLine();
            return new MusicCard
            {
                Artist = artist,
                Album = album,
                Comment = comment,
                Link = link
            };
        }
        public MusicCard UpdateMusicCardFromConsole(MusicCard mCard)
        {   
            Console.WriteLine("Artist: " + mCard.Artist);
            if (WantToChange())
                mCard.Artist = Console.ReadLine();
            
            Console.WriteLine("Album: " + mCard.Album);
            if (WantToChange())
                mCard.Album = Console.ReadLine();
            
            Console.WriteLine("Comment: " + mCard.Comment);
            if (WantToChange())
                mCard.Comment = Console.ReadLine();
            
            Console.WriteLine("Link: " + mCard.Link);
            if (WantToChange())
                mCard.Link = Console.ReadLine();   
                
            return mCard;
        }

        private bool WantToChange()
        {
            Console.WriteLine("Хотите изменить значение этого поля? Y/N");
            return Console.ReadLine() == "Y";
        }
    }
}
