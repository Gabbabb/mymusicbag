using System;
using CardId = System.Int32;

namespace MyMusicBagConsole
{
    public class App
    {
        private Provider _provider;
        private ConsoleInput _input;

        public App(Provider provider, ConsoleInput input)
        {
            this._provider = provider;
            this._input = input;
        }

        public void DoSmthWithConsoleInput(string[] args)
        {
            if (args[0] == "create")
            {
                MusicCard card = _input.CreateMusicCardFromConsole();
                _provider.Create(card);
                return;
            }

            if(args[0] == "read")
            {
                if(args[1] == "all")
                {
                    foreach (var card in _provider.ReadAll())
                        Console.WriteLine(card);
                }                   
                else
                {
                    CardId id = Int32.Parse(args[1]);
                    var mCard = _provider.Read(id);
                    Console.WriteLine(mCard);
                }                    
                return;
            }

            if(args[0] == "update")
            {
                CardId id = CardId.Parse(args[1]);
                var card = _provider.Read(id);
                _input.UpdateMusicCardFromConsole(card);
                _provider.Update(card);
                return;
            }

            if(args[0] == "delete")
            {
                CardId id = CardId.Parse(args[1]);
                _provider.Delete(id);
                return;
            }
        }
    }
}
