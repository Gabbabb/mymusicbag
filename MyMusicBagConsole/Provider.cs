using System.Collections.Generic;
using CardId = System.Int32;

namespace MyMusicBagConsole
{
    public class Provider
    {
        private MusicCardContext _context;
        public Provider()
        {
            _context = new MusicCardContext();
        }

        public void Create(MusicCard card)
        {
            _context.MusicCards.Add(card);
            _context.SaveChanges();            
        }
        public MusicCard Read(CardId id)
        {
           return _context.MusicCards.Find(id);
        }
        public void Update(MusicCard card)
        {
            _context.Update(card);
            _context.SaveChanges();
        }
        public void Delete(CardId id)
        {
            MusicCard card = new MusicCard
            {
                id = id
            };
            _context.MusicCards.Attach(card);
            _context.MusicCards.Remove(card);
            _context.SaveChanges();
        }

        public List<MusicCard>  ReadAll()
        {
            // у EF Core нет синхронного метода для чтения таблицы целиком, а захотелось все методы сделать синхронными
            // пришлось делать это
            List<MusicCard> cards = new List<MusicCard>();
            foreach(var card in _context.MusicCards)
                cards.Add(card);
            return cards;
        }
    }
}
